## Interview Training

<!--
## README FIRST

1. Title this issue with `Interview Training: [Team Member Name]`.
2. This issue should be remain confidential after creation, as the STAR interviewing training video linked below should be kept internal to GitLab as it contains confidential information.
3. Assign this issue to the person training.
4. Add Labels for the appropriate Division and Department the person in training is a member of.
-->

Before engaging candidates in a screening call or interview, please complete all of the following training.
**Note**: The videos linked are on [GitLab Unfiltered Youtube](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube) and will not be available to you unless **you are [logged into](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube) the Unfiltered Youtube account**.  

1. [ ] [Read interview process in the handbook](https://about.gitlab.com/handbook/hiring/interviewing/).
   1. [ ] Please confirm you understand that we do not ask for or record someone's pay history information. We only ask for and record *salary expectations*. Additionally, the candidate's salary expectations should only be stored in the Candidate's `Private Notes` tab in Greenhouse. Only recruiters should be talking about compensation information with candidates. 
1. [ ] Watch these training videos: 
   1. [ ] [Preparing for an interview](https://www.youtube.com/watch?v=k9qQ9uEiLvs) 
   1. [ ] [During the interview](https://www.youtube.com/watch?v=2POya3xr6fo) or read the [deck](https://docs.google.com/presentation/d/137QTrBBvM3Lfo_eO6yRSrWfZRJ_UWrRuvxzc1xwTIMg/edit?usp=sharing).
   1. [ ] [Behavioural/STAR interview](https://www.youtube.com/watch?v=cLus0qUfhro) or read the [deck](https://docs.google.com/presentation/d/1yYITcmXxrysO9RsI-tUgbmhVZW5Pb8IVaShBFJbWO2c/edit?usp=sharing).
   1. [ ] [Illegal interview questions](https://www.youtube.com/watch?v=6GpJ2K8l3uE) or read the [deck](https://docs.google.com/presentation/d/1QSvFI3PZ8NNnphzHzL5vOwTxuG54XZIiqvuz2OIbHQY/edit#slide=id.g7565b8f9f1_0_449).
1. [ ] Complete all modules of [Salesforce Equality at Work Training](https://trailhead.salesforce.com/trails/champion_workplace_equality).  To earn badges and save your responses, you'll need to sign up! Use your GitLab address to sign in using Google+.
   1. [ ] Once you've completed the units, from your Salesforce Dashboard, go to the "profile" tab, take a screenshot of your earned badges, and add that screenshot to the comments at bottom of this issue.
1. [ ] Review our [D&I Training and Learning Opportunities](https://about.gitlab.com/company/culture/inclusion/#diversity-inclusion--belonging-training-and-learning-opportunities) to learn more about how to be more inclusive. 
1. [ ] Watch this [training video](https://youtu.be/ca-FeCZMDto) on how to use Greenhouse as an Interviewer or Hiring Manager 
1. [ ] Read the Greenhouse [handbook page](https://about.gitlab.com/handbook/hiring/greenhouse/). 
1. [ ] Access your personal link in your [Zoom profile](https://zoom.us/profile).
    1. [ ] Please be sure that your personal link uses the GitLab convention as it is universal for the company and is used to schedule your interviews. Your personal link has to have the following convention: `https://gitlab.zoom.us/my/gitlab.firstnamelastname`. To customize it, go to Profile -> Personal Link -> Customize. 
    1. [ ] Please ensure you do not have a password enabled on your [personal link](https://about.gitlab.com/handbook/tools-and-tips/zoom/#making-a-customized-personal-link). 
    1. [ ] Please note this link can be joined by anyone at any time if they already have the link so it's a good idea to turn on the [waiting room function](https://about.gitlab.com/handbook/tools-and-tips/#enabling-the-waiting-room-for-your-personal-meeting-room). You can even customize the waiting room message with a GitLab logo and custom text!
1. [ ] Ensure your [office hours](https://support.google.com/calendar/answer/7638168?hl=en) are set up in your Google calendar.
1. [ ] Ensure that your [LinkedIn profile is up-to-date](https://about.gitlab.com/handbook/hiring/gitlab-ambassadors/#2-optimize-your-profiles). This along with your Slack photo and team page entry will be shared with candidates who reach the team interview stage.
1. [ ] Reach out to a member of your team to shadow at least two interviews. Remind the team member you are shadowing to email the candidate mentioning they will be shadowed by another GitLab team member and ask if the candidate is comfortable with that. We encourage you to collect feedback in addition to the interviewer during these sessions, so you can get used to taking notes that are impactful and relevant.
    1. [ ] Being an observer to the interview these notes could also be used to provide feedback to the interviewer as well as contribute to the hiring discussion of the candidate.  This should be provided to the interviewer for inclusion in the candidate's profile in Greenhouse.
1. [ ] Lead an interview, with a qualified interviewer shadowing you. Get feedback from the shadow after the interview. You are welcome to repeat this step until you are comfortable with leading.
1. [ ] Set up a personal reminder to be shadowed by an experienced interviewer after 3 months of interviewing.  This is essential to help us maintain high standards in our interviewing process. 

/label ~"Interview Training"
/confidential
