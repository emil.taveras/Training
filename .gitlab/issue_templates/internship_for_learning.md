<!--
## README FIRST

Title this issue with `Internship for Learning: [Team Member Name]`. Assign this issue to the person in the internship.
-->


This is meant for documenting an [Internship for Learning](https://about.gitlab.com/handbook/people-operations/promotions-transfers/#internship-for-learning).

## Intern

<!-- Add yourself here. -->

## Status

<!-- This is one of Planned, Ongoing, or Completed. -->

## Time period

<!-- Put start and end date here. Please remember to use ISO dates (https://about.gitlab.com/handbook/communication/#writing-style-guidelines). -->

## Percent of time

<!-- Specify the amount of time that is spent on the internship during the above time period. -->

## Goals to achieve / skills to acquire

<!-- This is typically (part of) the job responsibilities of the role you are interning in. -->

## Schedule

<!-- Set dates on the goals above or on concrete tasks here. If applicable link issues / merge requests that you work on. -->

## Results

<!-- You can optionally add achievements here that are not mentioned in goals or schedule. -->

## Involved Managers

<!-- Make sure to mention at least the manager of your current team and of the team you are interning in. -->

## Mentor / Buddy

<!-- Add a team member here who will guide you through the internship. -->

## Tasks

### Before the Internship

- [ ] Mention interning team member and job role in issue title.
- [ ] Talk to your current manager about the internship.
- [ ] Talk to the manager of the team your are interning in.
- [ ] Both Managers meet to discuss team member performance and agree upon the % of time the team member will commit to the internship
- [ ] Fill out above sections.
- [ ] Set status to "Planned".
- [ ] Create a merge request to update the [team directory](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/team_database.md) to add your internship as an additional role.
  - [ ] Ping your current manager in the merge request.
  - [ ] Assign the merge request to the manager of the team you are interning in.
  - [ ] Open an [access-request](https://gitlab.com/gitlab-com/access-requests/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) issue using the appropriate template for the team you're interning with, for temporary access to systems/services during your internship
- [ ] Mention this issue in your current team's Slack channel.
- [ ] Mention this issue in the Slack channel of the team your are interning in.

### During the Internship

Update the above sections (especially the status and schedule).

### After the Internship

- [ ] Leave a comment with your experience in this issue. (optional)
- [ ] Remove the internship from the [`team.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/team.yml).
- [ ] Close this issue.
- [ ] Thank everybody involved in the `#thanks` Slack channel for the opportunity. (optional)

/label ~"Internship for Learning"
/confidential
