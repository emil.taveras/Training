## Acting Manager at GitLab

<!--
## README FIRST

Title this issue with `Acting Manager: [Team Member Name]`. Assign this issue to the person training.
-->

The goal of this issue is to help you take on temporary duties and responsibilities of a manager and connect you with crucial information about being an [acting manager][acting-manager] at GitLab. Your first weeks as an acting manager can be exhilarating. It can also be challenging, especially when you need to quickly identify what is important for your success.

Items on this issue are identified as either required or suggested.  

  * **Required** items should be completed by all new GitLab managers.
  * **Suggested** items are optional. They may not be applicable to your style, team dynamic, or time you will be dedicated to the role.

Your contributions as an acting manager are essential. Submit a merge request to this issue template as ideas arise.

### Tasks

1. [ ] Schedule a handover meeting(s) with the current team manager. Try the 1:1:1 format for a transparent handoff. ([Instructions][transitioning-1-1-instructions], [Article][manager-handoff-worksheet] & [Template][handoff-template]). **REQUIRED**
    * Transition tasks that need coverage until we find a backfill for the role.
    * Shared documents, invitation to team meetings, and other things for you to know.
    * Professional goals and growth areas.
    * More recent feedback, projects, and other info since the last review cycle. *SUGGESTED*
1. [ ] Meet with your leader(s) and define or review measurable goals for your temporary role. **REQUIRED**
1. [ ] Schedule 1:1 meetings with team members ([Instructions][1-1-instructions] & [Template][1-1-agenda-format]). **REQUIRED**
1. [ ] Ensure that the [team page][updating-team-page] is properly updated with your new temporary role **REQUIRED**

### Support Networks

1. [ ] Join the [#managers Slack channel][slack-managers] and your department's Slack channel **REQUIRED**
1. [ ] Join the [#eng-week-in-review Slack channel][eng-week-in-review] **REQUIRED for Product Designers and Product Design Managers**
1. [ ] Join your department's Managers Meeting by asking for an invite from your leader(s) **REQUIRED**
1. [ ] Join the [Managers Lean Coffee Meeting][managers-lean-coffee] by asking for an invite in the #managers Slack channel. *SUGGESTED*
1. [ ] Schedule a coffee chat with the [People Business Partner][PBP] who supports your Department. You do not need to meet with your PBP in the first week, but you should schedule the chat during your first week. The chat should occur in the first 30 days. *SUGGESTED*
1. [ ] For GitLab team members moving into an acting managerial position, schedule a coffee chat with a team member that has gone trough the same experience. *SUGGESTED*
1. [ ] Discuss and practice the following topics with your mentor(s) or leader(s) *SUGGESTED*
    * Time management techniques
    * Decision making strategies

### Learning Modules

1. [ ] For GitLab team members moving into an acting managerial position: [Transitioning to a manager role][manager-development]. **REQUIRED**
1. [ ] Review leadership resources in the [Leadership Toolkit][leadership-toolkit] and spend time learning about *SUGGESTED*: 
   1. [ ] How to hold [coaching conversations][coaching]
   1. [ ] Enhancing [emotional intelligence][emotional-intelligence]
   1. [ ] [Career development][career-development]
   1. [ ] Review [how to be a great remote manager][remote-manager] page in our Handbook
   1. [ ] Read the [Understanding SOCIAl STYLEs][social-styles] Handbook page. Take the [free assessment][social-styles-assessmeet] and have your team complete the assessment. The more you know about your people the easier it will be to manage them. *SUGGESTED*
1. [ ] Try [writing in a journal][journal] to reflect on your experiences as acting manager *SUGGESTED*
1. [ ] Write and share a README file or "How I like to work" document for your team ([Examples][read-me-example]). *SUGGESTED*

----

### Other optional tasks

Depending how long you are required to occupy the role of acting manager and how your goals were defined, you might be required to become familar with and participate in a range of internal processes. All items below are **suggested**.

<details>
<summary>👉 Click to expand/contract</summary>

#### Team Tasks

1. [ ] Discuss and practice the following topics with your mentor(s) or leader(s)
    1. [ ] [Giving Performance Feedback][giving-feedback]
    1. [ ] [360 Survey Feedback Training][360-feedback]
    1. [ ] [360 Feedback Meetings][360-feedback-meeting]
1. [ ] Together with your leader, review and discuss career development plans with each team member ([Instructions][career-development] & [Template][career-development-template])

#### Learning Modules

1. [ ] [Career mapping at GitLab][career-development] / [Video][career-development-video]
      * [Engineering Career Development][eng-career-development]
      * [Marketing Career Development][mkt-career-development]
1. [ ] [Hiring process at GitLab][hiring]
    1. [ ] Ensure you have completed Interview Training. Your issue should have already been automatically opened, and can be found in the [Training Project][hiring-training].
    1. [ ] [New hire probationary period process][hiring-probation]
1. [ ] Schedule a coffee chat with [the recruiter(s)][recruiting-alignment] who supports your Department.
1. [ ] As acting manager, you might be required to onboard a new team member. Review the [onboarding page][team-onboarding] process. On the same page, review your part to the new team members onboarding process.
1. [ ] As acting manager, you might need to become familiar with the current offboarding process. Review the [offboarding page][offboarding] in the Handbook as well as the current Manager tasks in the [offboarding issue][offboarding-issue]. The offboarding tasks are time sensitive and the manager tasks need to be completed as soon as possible. 
1. [ ] Familiarize yourself on [compensation, benefits and equity at GitLab][total-rewards] as these topics will most likely come up in your conversations with your team members.
    1. [ ] Take the [Compensation Knowledge Assessment quiz][total-rewards-quiz]
    1. [ ] Take the [Benefits Knowledge Assessment quiz][benefits-quiz]
    1. [ ] Take the [Equity Knowledge Assessment quiz][equity-quiz]
1. [ ] Complete the correct level of anti-harassment training, if not reach out to `@jbandur` to request a new invite via WILL Learning. 

#### Other resources

1. [ ] [List of recommended leadership books][leadership-books] 
1. [ ] [Decision Making at GitLab][making-decisions]

</details>

<!-- Do not remove or edit the links below -->

[acting-manager]: https://about.gitlab.com/handbook/engineering/career-development/#acting-manager
[transitioning-1-1-instructions]: https://about.gitlab.com/handbook/leadership/1-1/#transitioning-1-1s
[manager-handoff-worksheet]: https://medium.com/making-meetup/the-manager-handoff-worksheet-c8acb2c899e6
[handoff-template]: https://docs.google.com/document/d/16f_Nrf4rpy6P5SDXxXn7f5q9zEmhIZdjI1Fqvic7498/edit?usp=sharing
[1-1-instructions]: https://about.gitlab.com/handbook/leadership/1-1/
[1-1-agenda-format]: https://about.gitlab.com/handbook/leadership/1-1/suggested-agenda-format/
[updating-team-page]: https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/website/#updating-the-team-page-and-org-chart
[stages-yml]: https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/stages.yml
[slack-managers]: https://gitlab.slack.com/messages/managers/
[eng-week-in-review]: https://gitlab.slack.com/messages/eng-week-in-review
[managers-lean-coffee]: https://gitlab.com/gitlab-com/people-group/leaders-leancoffee-topics/-/boards/965643
[PBP]: https://about.gitlab.com/handbook/people-group/#people-business-partner-alignment-to-division
[manager-development]: https://about.gitlab.com/handbook/people-operations/learning-and-development/manager-development/
[leadership-toolkit]: https://about.gitlab.com/handbook/people-group/leadership-toolkit/
[coaching]: https://about.gitlab.com/handbook/leadership/coaching/
[emotional-intelligence]: https://about.gitlab.com/handbook/people-group/learning-and-development/emotional-intelligence/
[career-development]: https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/
[remote-manager]: https://about.gitlab.com/company/culture/all-remote/being-a-great-remote-manager/
[social-styles]: https://about.gitlab.com/handbook/people-group/learning-and-development/emotional-intelligence/social-styles/#introducing-social-styles/
[social-styles-assessmeet]: https://about.gitlab.com/handbook/people-group/learning-and-development/emotional-intelligence/social-styles/#discover-your-social-style/
[journal]: https://hbr.org/2016/01/want-to-be-an-outstanding-leader-keep-a-journal
[read-me-example]: https://about.gitlab.com/handbook/engineering/readmes/
[career-development]: https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/#what-is-career-development
[career-development-template]: https://docs.google.com/document/d/1nBciVcCGXbeYKikufT_MZs1nn1iulKLOqXCgKaXDbQI/edit?usp=sharing
[giving-feedback]: https://about.gitlab.com/handbook/leadership/#giving-feedback
[360-feedback]: https://about.gitlab.com/handbook/people-group/360-feedback/#360-feedback
[360-feedback-meeting]: https://about.gitlab.com/handbook/people-group/360-feedback/#360-feedback-meeting
[career-development-video]: https://www.youtube.com/watch?v=YoZH5Hhygc4
[eng-career-development]: https://about.gitlab.com/handbook/engineering/career-development/
[mkt-career-development]: https://about.gitlab.com/handbook/marketing/career-development/
[hiring]: https://about.gitlab.com/handbook/hiring/
[hiring-training]: https://gitlab.com/gitlab-com/people-group/Training/-/issues
[hiring-probation]: https://about.gitlab.com/handbook/contracts/#probation-period
[recruiting-alignment]: https://about.gitlab.com/handbook/hiring/recruiting-alignment/
[team-onboarding]: https://about.gitlab.com/handbook/people-group/general-onboarding/
[offboarding]: https://about.gitlab.com/handbook/people-group/offboarding/
[offboarding-issue]: https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/master/.gitlab/issue_templates/offboarding.md
[total-rewards]: https://about.gitlab.com/handbook/total-rewards/
[total-rewards-quiz]: https://about.gitlab.com/handbook/total-rewards/compensation/#knowledge-assessment
[benefits-quiz]: https://about.gitlab.com/handbook/total-rewards/benefits/#knowledge-assessment
[equity-quiz]: https://about.gitlab.com/handbook/stock-options/#equity-knowledge-assessment
[leadership-books]: https://about.gitlab.com/handbook/leadership/#books
[making-decisions]: https://about.gitlab.com/handbook/leadership/#sts=Making%20Decisions
