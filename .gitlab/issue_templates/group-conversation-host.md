## Group Conversation Host Training

<!--
## README FIRST

Title this issue with `Group Conversation Host Training: [Team Member Name]`. Assign this issue to the person training.
-->

Group Conversations are company wide calls and although optional, highly important to keep the rest of GitLab informed of what your area of the business is working on.
We have a [six week rotation](https://about.gitlab.com/handbook/people-group/group-conversations/#schedule--dri), be sure to look at the GitLab Team Meetings Calendar, when you will be hosting.

Complete all following tasks prior to hosting a [Group Conversation](https://about.gitlab.com/handbook/people-group/group-conversations/).

### [Before the call](https://about.gitlab.com/handbook/people-group/group-conversations/#for-meeting-leaders)

- [ ] Read the [Group Conversation](https://about.gitlab.com/handbook/people-group/group-conversations/) section in the handbook.
- [ ] Read the [Preparation Tips](https://about.gitlab.com/handbook/people-group/group-conversations/#preparation-tips) and make sure you understand what is expected during the call. If you are uncertain, reach out to the `@people-exp` team in the #group-conversations slack channel.
- [ ] Watch  this [General (CEO) Group Conversation](https://youtu.be/wrnWaYS7Fgo) as an example of what a Group Conversation is like. Note: The video linked is on the GitLab Unfiltered YouTube channel, but it is Public so anyone can access it. To view Private videos such as [this one](https://www.youtube.com/watch?v=owmdOI82WGs), you need to have been given Manager access and be signed into GitLab Unfiltered to be able to watch. Please ping `@people_exp` in the #peopleops Slack channel if your invitation has expired. It is found in the Pending Invitations section [here](https://myaccount.google.com/brandaccounts).

### [During the call](https://about.gitlab.com/handbook/people-group/group-conversations/#during-the-call)

- [ ] This is similar to an AMA in the sense that you will be answering loads of questions during the call.
- [ ] Do not present your slides during the call, as per the handbook, if you want to present [please consider posting a recording to YouTube](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/) at least a day before the meeting, link it from the Google Doc, and mention it in the relevant slack channels.

### Other Important things to note

- [ ] Read through the [other things to note section](https://about.gitlab.com/handbook/people-group/group-conversations/#other-things-to-note) about Group Conversations at GitLab.